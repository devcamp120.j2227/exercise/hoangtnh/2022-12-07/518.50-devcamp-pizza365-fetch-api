import { Component } from "react";

class Pizza365FetchAPI extends Component{
    fetchApi = async(url, body) => {
        const response = await fetch(url,body);
        const data = await response.json();
        return data;
    }

    //function get all order
    getAllBtnHandler = () =>{
        this.fetchApi("http://localhost:8000/api/orders")
            .then((data) =>{
                console.log(data)
            })
            .catch((error)=>{
                console.log(error.message)
            })
        
    }
    createBtnHandler = () => {
        const body = {
            method: "POST",
            body: JSON.stringify({
                _id: 2,
                orderCode: "GLMRN2022",
                pizzaSize: "L",
                pizzaType: "Seafood",
                voucher:"6373b9d6e0bae0ea8a36a042",
                drink:"Coke",
                status:"Open"
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        this.fetchApi("http://localhost:8000/api/users/637dc9809055ad146d5efa6c/orders", body)
            .then((data) =>{
                console.log(data)
            })
            .catch((error)=>{
                console.log(error.message)
            })
    }
    getOrderByIdBtnHandler =() =>{
        const orderId = "63813275854a73f7ba85956d"
        this.fetchApi("http://localhost:8000/api/orders/"+ orderId)
            .then((data)=>{
                console.log(data)
            })
            .catch((error)=>{
                console.log(error.message)
            })
    }
    updateBtnHandler = () => {
        const orderId = "63813275854a73f7ba85956d"
        const body = {
            method: "PUT",
            body: JSON.stringify({
                _id: 133,
                orderCode: "GLMRN20221",
                pizzaSize: "M",
                pizzaType: "Seafood",
                voucher:"6373b9d6e0bae0ea8a36a042",
                drink:"Coke",
                status:"Open"
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        this.fetchApi("http://localhost:8000/api/orders/" + orderId, body)
            .then((data) =>{
                console.log(data)
            })
            .catch((error)=>{
                console.log(error.message)
            })
    }
    checkVoucherBtnHandler = () =>{
        const voucherId = "6373badcdcd3ebe07ce5fbae"
        this.fetchApi("http://localhost:8000/api/vouchers/"+ voucherId  )
            .then((data)=>{
                console.log(data)
            })
            .catch((error)=>{
                console.log(error.message)
            })
    }
    getDrinkBtnHandler = () => {
        this.fetchApi ("http://localhost:8000/api/drinks")
            .then((data)=>{
                console.log(data)
            })
            .catch((error) =>{
                console.log(error.message)
            })
    }
    render() {
        return (
            <div>
                <div className="mt-5">
                    <p>
                        Test Page for Javascript Tasks. F5 to run code
                    </p>
                </div>
                <div className="row ">
                    <div className="col-2">
                        <button className="btn btn-primary" onClick={this.getAllBtnHandler}>Call api get all orders</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-info" onClick={this.createBtnHandler}>Call api create order</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-success" onClick={this.getOrderByIdBtnHandler}>Call api get order by id</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-secondary" onClick={this.updateBtnHandler}>Call api update order</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-danger" onClick={this.checkVoucherBtnHandler}>Call api check voucher by id</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-success" onClick={this.getDrinkBtnHandler}>Call api get drink list</button>
                    </div>
                </div>
                <div className="mt-5">
                    <p><i>Demo 6 API for Pizza 365 project:</i></p>
                    <li>Get all orders: lấy tất cả orders</li>
                    <li>Create order: tạo 1 order mới</li>
                    <li>Get order by ID: lấy 1 order bằng ID</li>
                    <li>Update order: update 1 order bằng ID</li>
                    <li>Check voucher by ID: check thông tin mã giảm giá, quan trọng là có hay không và % giảm giá</li>
                    <li>Get drink list: lấy danh sách đồ uống</li>
                </div>
                <div className="mt-3">
                    <p className="text-danger"><b>Bật console log để nhìn rõ output</b></p>
                </div>
            </div>
        )
    }
}
export default Pizza365FetchAPI;